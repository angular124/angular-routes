import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  urls=[
    {
    link:"",
    title:"Home",
  },
  {
    link:"/contacts",
    title:"Contacts",
  },
  {
    link:"/users",
    title:"Users",
  },
  {
    link:"/father_son",
    title:"Father/Son",
  },
]
  constructor() { }

  ngOnInit(): void {
  }

}
