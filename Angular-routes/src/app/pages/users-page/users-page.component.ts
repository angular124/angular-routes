import { Component, OnInit } from '@angular/core';
import { FirstServiceService } from 'src/app/services/firstService/first-service.service';

export const globalUsers=[
  {id:1,name:"Abel", role:"Profesor"},
  {id:2,name:"Jose", role:"Profesor"},
  {id:3,name:"Hector", role:"Profesor"},
  {id:4,name:"Lucas", role:"Profesor"},
  {id:5,name:"Carlos", role:"Profesor"},

]
@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {
// data:any



  constructor(public service:FirstServiceService) { }

  ngOnInit(): void {
    this.service.getInformationObservable().subscribe(dat=>{console.log("Reciving DATA",dat);
    // this.data=dat
    })

  }
  users=globalUsers
}
