import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { globalUsers } from '../../users-page.component';

@Component({
  selector: 'app-users-detail-page',
  templateUrl: './users-detail-page.component.html',
  styleUrls: ['./users-detail-page.component.scss']
})
export class UsersDetailPageComponent implements OnInit {
userDetails:any={}
  constructor(private routes:ActivatedRoute) { }

  ngOnInit(): void {
    this.routes.paramMap.subscribe(params=>{
      const userId=params.get("userId"); //Debe coincidir el params con el de la ruta dinamica
      console.log(userId);

      //Simulacion.deberia ser un fetch
      this.userDetails=globalUsers.find(user => user.id.toString() === userId);//Busca en el fetch el ID coincidente para mostrar dicho usuario

    })
  }

}
