import { FirstServiceService } from './../../../services/firstService/first-service.service';
import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventEmitter } from '@angular/core';


@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
@Output() sendForm=new EventEmitter<object>();
@Output() emitMessage=new EventEmitter<string>()
@Input() inputTextSon:string=""
// data:any
  //FORMULARIOS REACTIVOS
  contactForm:FormGroup;
  submitted:boolean=false;
  constructor(
    private formBuillder:FormBuilder,
    private service:FirstServiceService) {
    this.contactForm=this.formBuillder.group(
      {
        name:["",[Validators.required, Validators.minLength(4)]],
        email:["",[Validators.required, Validators.email]],
        description:[""],

      }
    );
    //controla si el formulario es valido o no en su "status"
    this.contactForm.statusChanges.subscribe(status=>{
      console.log(status);
    })
 //controla los cambios en el formulario "changes"
      // this.contactForm.valueChanges.subscribe(changes=>{
      //   console.log(changes);
      // })
  }
  ngOnInit(): void {
  }
onSubmit(){
  this.submitted=true;
  console.log(this.contactForm);
  // Output uqe envia servicio al de hijo a padre
  this.sendForm.emit(this.contactForm.value);
  console.log("Se emite =>", this.contactForm.value);
  // Servicio para enviar de un componente a otro(funcion de salida)
  this.service.sendInformationObservable(this.contactForm.value)
}

// Outpue que envia datos al padre
message:any=""
inputMessage(){
  this.emitMessage.emit(this.message)
}
}
