import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { ContactPageComponent } from './pages/about-page/contact-page/contact-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { UsersDetailPageComponent } from './pages/users-page/pages/users-detail-page/users-detail-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from './core/components/menu/menu.component';
import { FirstServiceService } from './services/firstService/first-service.service';

@NgModule({
  declarations: [
    AppComponent,
    UsersPageComponent,
    ContactPageComponent,
    HomePageComponent,
    AboutPageComponent,
    UsersDetailPageComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,

  ],
  providers: [FirstServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
