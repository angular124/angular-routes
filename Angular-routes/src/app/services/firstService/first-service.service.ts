import { Injectable, Output , EventEmitter} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable(
)

export class FirstServiceService {
  // Tuberia de conexion
private information:BehaviorSubject<any>=new BehaviorSubject(null);
public list:any[]=[]
  constructor() { }
// funcion que recoge informacion de la tuberia
  getInformationObservable(): Observable<any>{
    return this.information.asObservable();
  }
  // funcion que envia informacion de la tuberia
  sendInformationObservable(information:any): void{
    console.log("Enviando desde servicio",information);
    this.list.push(information)
    this.information.next(information)
  }

}

// getProfileObs(): Observable<Profile> {
//         return this.profileObs$.asObservable();
//     }

//     setProfileObs(profile: Profile) {
//         this.profileObs$.next(profile);
//     }

