import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { ContactPageComponent } from './pages/about-page/contact-page/contact-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { UsersDetailPageComponent } from './pages/users-page/pages/users-detail-page/users-detail-page.component';
import { UsersPageComponent } from './pages/users-page/users-page.component';


const routes: Routes = [
  {path:"", component:HomePageComponent},
  {path:"users", component:UsersPageComponent},
  {path:"users/:userId", component:UsersDetailPageComponent},
  {path:"contacts", component:ContactPageComponent},
  {path:"father_son", component:AboutPageComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
